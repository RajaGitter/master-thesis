<?xml version="1.0" ?>
<sdf version="1.4">
  <world name="default">

    <!-- A global light source -->
    <light name='sun' type='directional'>
      <cast_shadows>1</cast_shadows>
      <pose frame=''>0 0 10 3.14 3.14 0</pose>
      <diffuse>0.8 0.8 0.8 1</diffuse>
      <specular>0.1 0.1 0.1 1</specular>
      <attenuation>
        <range>1000</range>
        <constant>0.9</constant>
        <linear>0.01</linear>
        <quadratic>0.001</quadratic>
      </attenuation>
      <direction>-0.5 0.5 -1</direction>
    </light>

    <!-- A ground plane -->
    <include>
      <uri>model://ground_plane</uri>
    </include>

    <!-- Wall -->
    <model name='wall'>

      <link name='back_wall'>
        <pose frame=''>1.1 0 2 0 0 0</pose>
        <inertial>
          <mass>10</mass>
          <inertia>
            <ixx>0.166667</ixx>
            <ixy>0</ixy>
            <ixz>0</ixz>
            <iyy>0.166667</iyy>
            <iyz>0</iyz>
            <izz>0.166667</izz>
          </inertia>
        </inertial>
        <collision name='collision'>
          <geometry>
            <box>
              <size>0.2 4 4</size>
            </box>
          </geometry>
          <surface>
            <friction>
              <ode>
                <mu>1.16</mu>
                <mu2>1.16</mu2>
                <slip1>0</slip1>
                <slip2>0</slip2>
              </ode>
            </friction>
          </surface>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='visual'>
          <geometry>
            <box>
              <size>0.2 4 4</size>
            </box>
          </geometry>
          <material>
            <script>
              <name>Gazebo/White</name>
              <uri>file://media/materials/scripts/gazebo.material</uri>
            </script>
          </material>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
      </link>

      <joint name='ground_link' type='fixed'>
        <parent>world</parent>
        <child>back_wall</child>
        <pose frame=''>0 0 0 0 -0 0</pose>
      </joint>

      <!--<plugin name="f3d_controller" filename="libgazebo_ros_f3d.so">
               <alwaysOn>true</alwaysOn>
               <updateRate>50.0</updateRate>
               <bodyName>back_wall</bodyName>
               <topicName>wall_force</topicName>
      </plugin>-->

      <!--<link name='front_wall'>
        <pose frame=''>1.25 0 2 0 0 0</pose>
        <inertial>
          <mass>10</mass>
          <inertia>
            <ixx>0.166667</ixx>
            <ixy>0</ixy>
            <ixz>0</ixz>
            <iyy>0.166667</iyy>
            <iyz>0</iyz>
            <izz>0.166667</izz>
          </inertia>
        </inertial>
        <collision name='collision'>
          <geometry>
            <box>
              <size>0.1 4 4</size>
            </box>
          </geometry>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='visual'>
          <geometry>
            <box>
              <size>0.1 4 4</size>
            </box>
          </geometry>
          <material>
            <script>
              <name>Gazebo/White</name>
              <uri>file://media/materials/scripts/gazebo.material</uri>
            </script>
          </material>
        </visual>
        <self_collide>0</self_collide>
        <kinematic>0</kinematic>
      </link>

      <joint name='ground_link2' type='fixed'>
        <parent>world</parent>
        <child>front_wall</child>
        <pose frame=''>0 0 0 0 -0 0</pose>
      </joint>

      <joint name="force_sensor" type="revolute">
        <parent>back_wall</parent>
        <child>front_wall</child>
        <pose>0.1 0 0 0 0 0</pose>
        <axis>
          <limit>
            <lower>-1</lower>
            <upper>1</upper>
          </limit>
          <dynamics>
            <damping>0.000000</damping>
            <friction>0.000000</friction>
          </dynamics>
          <xyz>1.000000 0.000000 0.000000</xyz>
        </axis>
        <sensor name="force_torque" type="force_torque">
          <update_rate>100.0</update_rate>
        </sensor>
      </joint>

      <plugin name="FTsensor_controller" filename="libgazebo_ros_ft_sensor.so">
      <robotNamespace>wall</robotNamespace>
        <updateRate>50.0</updateRate>
        <topicName>force_torque</topicName>
        <gaussianNoise>0.0</gaussianNoise>
        <jointName>force_sensor</jointName>
      </plugin>-->

    </model>

    <!-- Only one ROS interface plugin is required per world, as any other plugin can connect a Gazebo
         topic to a ROS topic (or vise versa). -->
    <plugin name="ros_interface_plugin" filename="librotors_gazebo_ros_interface_plugin.so"/>

    <physics name='default_physics' default='0' type='ode'>
      <gravity>0 0 -9.8066</gravity>
      <ode>
        <solver>
          <type>quick</type>
          <iters>10</iters>
          <sor>1.3</sor>
          <use_dynamic_moi_rescaling>0</use_dynamic_moi_rescaling>
        </solver>
        <constraints>
          <cfm>0</cfm>
          <erp>0.2</erp>
          <contact_max_correcting_vel>100</contact_max_correcting_vel>
          <contact_surface_layer>0.001</contact_surface_layer>
        </constraints>
      </ode>
      <max_step_size>0.002</max_step_size>
      <real_time_factor>2</real_time_factor>
      <real_time_update_rate>1000</real_time_update_rate>
      <magnetic_field>6e-06 2.3e-05 -4.2e-05</magnetic_field>
    </physics>

    <gui fullscreen='0'>
      <camera name='user_camera'>
        <pose frame=''>-2 -4 3 0 0.39270 0.78534</pose>
      </camera>
    </gui>

  </world>
</sdf>
